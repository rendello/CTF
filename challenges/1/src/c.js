

function get_key_from_form() {
	var arr = ["c_1", "c_2", "c_3", "c_4", "c_5", "c_6", "c_7", "c_8", "c_9"];
	var key = 0

	arr.forEach((item, index)=>{
		item = document.getElementById(item)
		if (item.checked) {
			key += parseInt(item.value)
		}
	});
	return key
}


function get_available_chars() {
	var char_ranges = {
		'latin': [32, 127],
		'cyrillic': [1024, 1279],
		'greek': [931, 1023],
		'ipa': [592, 683]
	}
	var chars = []
	for (var key in char_ranges) {
		lower_bound = char_ranges[key][0]
		upper_bound = char_ranges[key][1]

		for (i=lower_bound; i <= upper_bound; i++) {
			chars.push(String.fromCharCode(i))
		}
	}
	return chars
}


function decrypt(text, key) {
	var decrypted = ''
	for (let c = 0; c < text.length; c++) {
		if (c % 2) {
			decrypted += String.fromCharCode(text[c].charCodeAt(0) + key - (c * 10))
		} else {
			decrypted += String.fromCharCode(text[c].charCodeAt(0) - (key * key) + (c ^ 2))
		}
	}
	return decrypted
}


function do_the_thing() {
	key = get_key_from_form()
	document.getElementById("result").innerHTML = decrypt('᧬ǌ᧫ǖ᧟ǅ᧤ȇ᧵Ȗ᧧Ȧ᧯Ʉ', key)
}

