
My favourite puzzle.

At the end of the large ASCII image, these lines exist:

```
BEST EDITOR =
jyyj5pkyypppp4k^13x$xxxxxxxxxxxj^13xld$Jxjjddkddk^2ld$kddpkJ^2lxA! LOL

abcdefghijklmnopqrstuvwxyz
```

The hint is in the question itself. A favourite editor to many hacker-types is
Vim. And if you go into Vim and put your cursor on the line starting with `jyyj5`
and manually type the presented keys in order (or, more quickly, type `yy;@"` to
run the line automatically), the alphabet below gets mutated into the flag:

```
nano! LOL
```
