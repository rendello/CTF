
Another fairly simple one hidden in plain sight.

In the first few lines of the main page, there is this tag:

`<link rel="license" href="license.txt">`

It's a meta-tag, and I'm pretty sure using those for licenses is non-standard.
Anyway, if you look inside, it's a standard BSD 3-Clause license (my favourite)
with this extra string added at the end:

```
BY THE WAY, ARE YOU "WORTH YOUR SALT".
```
