
The original puzzle that kicked everything off. The most complex puzzle, but
depending on how you go about it, not hard to crack.

On opening the page, the first thing seen is a 3x3 square of checkboxes and an
odd string of text below it:

<pre>
								 +---+---+---+
								 |   |   |   |
								 +---+---+---+
								 |   |   |   |
								 +---+---+---+
								 |   |   |   |
								 +---+---+---+

								ňKlIck+MĚÍŧÜŢâĕ
</pre>

If you click the text, it transforms into something completely different. If you
change the values of the checkboxes, the text will transform into a different,
unique garbled form.

The checkboxes are providing a numerical key to decrypt the text, and the user
has to configure the key correctly to see the flag. There are many ways to go
about this:

## 1: Brute-forcing the box itself

In the box, there are 9 checkboxes with 2 possible values (checked or unchecked)
each. That means there are (2^9=) 512 different combinations possible. A user
could try these out manually, or write a program to check and uncheck the boxes.
But why do that when there's another option:

## 2: Brute-force the keys

To decrypt the keys, the code is running a function called decrypt. The first
value is the string '᧬ǌ᧫ǖ᧟ǅ᧤ȇ᧵Ȗ᧧Ȧ᧯Ʉ', and the second value is a numerical key.
You see, the checkboxes each have a numerical value. The sum of all boxes (which
is always unique, due to the pattern of their numerical values) is the key for
the decryption.

But if it's a numerical key, and we know the encrypted string, there's no reason
not to try every possible key in a large range and just look at the returned
values, right? This code does just that.

```
for (var i = 0; i < 1000; i++) {console.log(decrypt('᧬ǌ᧫ǖ᧟ǅ᧤ȇ᧵Ȗ᧧Ȧ᧯Ʉ', i))}
```

It's using the already-loaded decrypt funcion and trying 100 permutations. All a
user has to do is search for the known text in the flags and find the
unencrypted result.

## 3: Reversing the decryption

This is overkill. As you may have guessed, the encryption is a variant of ROT,
where each character is shifted by a certain amount in the alphabet (or in this
case, all of Unicode). My encryption is a bit stronger, as it shifts differently
depending on where the character is in the word.

If someone wants to reverse the decryption scheme, all they have to do is look
in the `jquery.js` file. The decrypt function in `c.js` is a dud and is
overwritten when jquery is loaded.

The final key is:

```
CODE: ANTIHERO
```
