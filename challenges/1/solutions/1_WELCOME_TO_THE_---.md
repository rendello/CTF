
The `WELCOME TO THE _` flag should be the easiest one to get, although that
doesn't mean it's always the first.

Open the main page in the web browser, then look at the page's source. Near the
end of the file, there's a few hundred blank lines before the closing `</body>`
tag. On of these lines contains the comment:

```
WELCOME TO THE CYBER ELITE
```
