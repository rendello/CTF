
This flag is probably the first one that most people search for.

In the giant background ASCII-art 'image', there are hidden multiple alphabetic
letters among the various symbols. Putting them all together renders:

```
WHEN DO I STOP
```
