
# CTF № 1: IDENTITY UNKNOWN; WE NEED A HERO
link: https://rendello.ca/ctf/1

This CTF started 2019-10-26 and ended on 2019-10-28, with Dragonna winning
(first to reach all 35 points).

![Spooky Screaming Man](scream.png "Spooky Screaming Man")
