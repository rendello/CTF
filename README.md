
# RENDELLO CTFs
Welcome! Here, I host the code and solutions to my CTF challenges that I host on [rendello.ca/ctf](https://rendello.ca/ctf).


| № | Title                            | Start      | End        | Top 5 players at finish                           |
|---|----------------------------------|------------|------------|---------------------------------------------------|
| 1 | IDENTITY UNKNOWN; WE NEED A HERO | 2019-10-26 | 2019-10-28 | Dragonna : 35<br> SigSegV  : 35<br> ptitpou  : 23 |
